UPDATE inventorytran SET unik='Error'
where client1='KORBUS' and   Code = 'orChange'
AND    stat = 'Shipp' and unik='N' and fert_id in (
SELECT --It.client1,
       It.fert_id
       --MIN(It.Key) AS Key,
       --o.Shipp_date
FROM   hr.inventorytran It,
       hr.orde          o
WHERE  It.client1 = 'KORBUS'
AND    It.Code = 'orChange'
AND    It.stat = 'Shipp'
      /*AND ith.unik = 'N'*/
AND    o.Status = 'Shipp'
AND    o.client1 = It.client1
AND    o.or_id = It.fert_id
AND    o.Order_Type <> 'COR'
AND    o.uf7 <> 'UMB'
AND    (NOT EXISTS (SELECT 1
                    FROM   hr.iloine ilo
                    WHERE  ilo.client1 = It.client1
                    AND    ilo.or_id = It.fert_id
                    AND    ilo.shipped > 0) OR NOT EXISTS
       (SELECT 1
         FROM   hr.iloine ilo
         WHERE  ilo.client1 = It.client1
         AND    ilo.or_id = It.fert_id
         AND    ilo.shipped > 0
         AND    ilo.Line_Id NOT IN
                (SELECT hop.Line_Id
                  FROM   hr.ins_spec hop
                  WHERE  hop.client1 = ilo.client1
                  AND    hop.fert_id = ilo.or_id
                  AND    hop.Line_Id = ilo.Line_Id)))
AND EXISTS (SELECT 1 FROM hr.iloine ilo
WHERE  ilo.client1 = It.client1
                    AND    ilo.or_id = It.fert_id
                    AND    ilo.shipped is NULL)
 
AND    It.unik = 'N'
AND o.Shipp_date < SYSDATE -2/24
GROUP  BY It.client1,
          It.fert_id,
          o.shipp_date);
	
	
	

